import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import List from './src/list';
import Details from './src/details';
import HomeScreen from './src/home';
import TaskList from './src/taskList';
import TaskDetails from './src/taskDetails';
import Gestos from './src/gestos';
import CountMoviment from './src/countMoviment';


const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='HomeScreen'>
        <Stack.Screen name='HomeScreen' component={HomeScreen}/>
        <Stack.Screen name='TaskList' component={TaskList}/>
        <Stack.Screen name='TaskDetails' component={TaskDetails}/>
        <Stack.Screen name='List' component={List}/>
        <Stack.Screen name='Details' component={Details} />
        <Stack.Screen name='Gestos' component={Gestos} />
        <Stack.Screen name='Count' component={CountMoviment} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
