import React from "react";
import { View, Text, TouchableOpacity, FlatList, StyleSheet } from "react-native";

const TaskList = ({ navigation }) => {
  const tasks = [
    {
      id: 1,
      title: "Fazer lição de história",
      date: "2024-02-27",
      time: "16:20",
      address: "Sesi",
    },
    {
        id: 2,
        title: "Ir ao mercado",
        date: "2024-02-28",
        time: "09:00",
        address: "Rua das Flores, 123",
      },
      {
        id: 3,
        title: "Ligar para o cliente",
        date: "2024-02-27",
        time: "18:30",
        address: "Telefone",
      },
      {
        id: 4,
        title: "Estudar para o exame",
        date: "2024-02-28",
        time: "14:00",
        address: "Biblioteca Municipal",
      },
      {
        id: 5,
        title: "Fazer compras online",
        date: "2024-02-29",
        time: "10:00",
        address: "N/A",
      },
      {
        id: 6,
        title: "Enviar relatório",
        date: "2024-02-27",
        time: "20:00",
        address: "Email",
      },
      {
        id: 7,
        title: "Marcar consulta médica",
        date: "2024-03-01",
        time: "11:30",
        address: "Clínica Central",
      },
      {
        id: 8,
        title: "Preparar apresentação",
        date: "2024-02-29",
        time: "15:00",
        address: "Em casa",
      },
      {
        id: 9,
        title: "Limpar o carro",
        date: "2024-03-02",
        time: "08:00",
        address: "Garagem",
      },
      {
        id: 10,
        title: "Enviar convites para festa",
        date: "2024-02-29",
        time: "18:00",
        address: "Email",
      }
  ];
  function taskPress(task){
    navigation.navigate('TaskDetails', {task})
  }
  return (
    <View style={styles.container}>
      <FlatList 
      data={tasks}
      keyExtractor={(item)=> item.id.toString}
      renderItem={({item})=>(
        <TouchableOpacity style={styles.touchable} onPress={() => taskPress(item)}>
            <Text style={styles.textTouchable}>{item.title}</Text>
        </TouchableOpacity>
      )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
    text: {
      fontWeight: 'bold', 
      textAlign: 'center', 
      fontSize: 24, 
      padding: 30
    }, 
    touchable: {
        flex: 1,
        borderWidth: 1,
        borderColor: '#7A7A7A',
        width: 200, 
        height: 30,
        margin: 'auto',
        marginTop: 10,
        borderRadius: 3, 
        padding: 3,
    },
    textTouchable: {
        fontWeight: '500', 
        textAlign: 'center',
    },  
  });
export default TaskList;