import React, { useEffect, useState } from "react";
import { StyleSheet, View, Text, FlatList, TouchableOpacity } from "react-native";
import axios from "axios";

const List = ({navigation}) => {
  const [users, setUsers] = useState([]);
  useEffect(() => {
    getUsers();
  }, []);

  async function getUsers() {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      setUsers(response.data);
    } catch (error) {
      console.error(error);
    }
  }

  function userPress(user) {
    navigation.navigate("Details", { user });
  }

  return (
    <View style={styles.container}>
      <FlatList
        data={users}
        keyExtractor={(item) => item.id.toString}
        renderItem={({ item }) => (
          <TouchableOpacity style={styles.touchable} onPress={() => userPress(item)}>
            <Text style={styles.textTouchable}>{item.name}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
    touchable: {
        flex: 1,
        borderWidth: 1,
        borderColor: '#7A7A7A',
        width: 200, 
        height: 30,
        margin: 'auto',
        marginTop: 10,
        borderRadius: 3, 
        padding: 3,
    },
    textTouchable: {
        fontWeight: '500', 
        textAlign: 'center',
    },  
  });
  
  
export default List;
