import React from "react";
import { StyleSheet, View, Text, ScrollView } from "react-native";

const Details = ({ route }) => {
    const { user } = route.params;
    return(
    <View style={styles.container}>
        <ScrollView>
            <Text style={styles.title}>{user.name}</Text>
            <Text style={styles.text}><Text style={styles.titleOne}>Id:</Text> {user.id}</Text>
            <Text style={styles.text}><Text style={styles.titleOne}>Username:</Text> {user.username}</Text>
            <Text style={styles.text}><Text style={styles.titleOne}>Email:</Text> {user.email}</Text>
            <Text style={styles.text}><Text style={styles.titleOne}>WebSite:</Text> {user.website}</Text>
            <Text style={styles.text}><Text style={styles.titleOne}>Telefone:</Text> {user.phone}</Text>
            <Text style={styles.text}><Text style={styles.titleOne}>Endereço</Text></Text>
            <Text style={styles.text}><Text style={styles.titleOne}>Street:</Text> {user.address.street}</Text>
            <Text style={styles.text}><Text style={styles.titleOne}>Suite:</Text> {user.address.suite}</Text>
            <Text style={styles.text}><Text style={styles.titleOne}>City:</Text> {user.address.city}</Text>
            <Text style={styles.text}><Text style={styles.titleOne}>Zipcode:</Text> {user.address.zipcode}</Text>
            <Text style={styles.text}><Text style={styles.titleOne}>Latitude:</Text> {user.address.geo.lat}</Text>
            <Text style={styles.text}><Text style={styles.titleOne}>Longitude:</Text> {user.address.geo.lng}</Text>
            <Text style={styles.text}><Text style={styles.titleOne}>Company</Text></Text>
            <Text style={styles.text}><Text style={styles.titleOne}>Longitude:</Text> {user.company.name}</Text>
            <Text style={styles.text}><Text style={styles.titleOne}>Catch Phrase:</Text> {user.company.catchPhrase}</Text>
            <Text style={styles.text}><Text style={styles.titleOne}>BS:</Text> {user.company.bs}</Text>
        </ScrollView>
    </View>
    );
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
      },
    title: {
        fontWeight: '600',
        textAlign: 'center',
        padding: 20, 
        fontSize: 20
    }, 
    titleOne: {
        fontWeight: '600', 
        textAlign: 'center'
    },
    text: {
        borderWidth: 1,
        borderColor: '#3A356E',
        width: 230, 
        height: 30,
        margin: 'auto',
        marginTop: 10,
        borderRadius: 3,
        textAlign: 'center', 
        padding: 5
    }
  });

export default Details;