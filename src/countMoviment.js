import React, { useRef, useState } from "react";
import { View, Dimensions, PanResponder, Text } from "react-native";
import Svg, { Line } from "react-native-svg";

const CountMoviment = () => {
  const [count, setCount] = useState(0);
  const [startCoordinates, setStartCoordinates] = useState({ x: 0, y: 0 });
  const [endCoordinates, setEndCoordinates] = useState({ x: 0, y: 0 });
  const screenWidth = Dimensions.get("window").width;
  const gestureThreshold = screenWidth * 0.25;

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderGrant: (event, gestureState) => {
        const { x0, y0 } = gestureState;
        setStartCoordinates({ x: x0, y: y0 });
        setEndCoordinates({ x: x0, y: y0 });
      },
      onPanResponderMove: (event, gestureState) => {
        const { x0, y0, dx, dy } = gestureState;
        setEndCoordinates({ x: x0 + dx, y: y0 + dy });
      },
      onPanResponderRelease: () => {
        setCount((prevCount) => prevCount + 1);
        setStartCoordinates({ x: 0, y: 0 });
        setEndCoordinates({ x: 0, y: 0 });
      },
    })
  ).current;

  return (
    <View
      style={{
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#000000",
      }}
      {...panResponder.panHandlers}
    >
      <Svg
        style={{ position: "absolute", top: 0, left: 0 }}
        width={screenWidth}
        height={Dimensions.get("window").height}
      >
        <Line
          x1={startCoordinates.x}
          y1={startCoordinates.y}
          x2={endCoordinates.x}
          y2={endCoordinates.y}
          stroke="#00FF00" // Cor verde do sabre de luz
          strokeWidth={5}
          strokeLinecap="round"
        />
      </Svg>
      <Text style={{ color: "#FFFFFF", marginTop: 20 }}>
        Valor do Contador: {count}
      </Text>
    </View>
  );
};

export default CountMoviment;
