import React from "react";
import { View, Text, StyleSheet } from "react-native";

const TaskDetails = ({ route }) => {
    const { task } = route.params;
    return(
    <View style={styles.container}>
        <Text style={styles.text}> <Text style={styles.titleOne}>Nome da Tarefa:</Text> {task.title}</Text>
        <Text style={styles.text}><Text style={styles.titleOne}>Data:</Text>{task.date}</Text>
        <Text style={styles.text}><Text style={styles.titleOne}>Hora:</Text>{task.time}</Text>
        <Text style={styles.text}><Text style={styles.titleOne}>Local:</Text> {task.address}</Text>
    </View>
    );
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
      },
    titleOne: {
        fontWeight: '600', 
        textAlign: 'center'
    },
    text: {
        borderWidth: 1,
        borderColor: '#3A356E',
        width: 280, 
        height: 30,
        margin: 'auto',
        marginTop: 10,
        borderRadius: 3,
        textAlign: 'center', 
        padding: 5
    }
  });

export default TaskDetails;