import React from 'react';
import { View, Button, TouchableOpacity, Text, StyleSheet} from "react-native";
import { useNavigation } from '@react-navigation/native'

const HomeScreen = () => {

    const navigation = useNavigation();

    const handleUsers = () => {
        navigation.navigate('List')
    }

    const handleTasks = () => {
        navigation.navigate('TaskList')
    }
    const handleGestos = () => {
        navigation.navigate('Gestos')
    }
    const handleTratamentoGestos = () => {
        navigation.navigate('Count')
    }

    return(
        <View style={styles.container}>
            <TouchableOpacity style={styles.touchable} onPress={handleUsers}>
                <Text style={styles.textTouchable}>Usuários</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchable} onPress={handleTasks}>
                <Text style={styles.textTouchable}>Tarefas</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchable} onPress={handleGestos}>
                <Text style={styles.textTouchable}>Gestos</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchable} onPress={handleTratamentoGestos}>
                <Text style={styles.textTouchable}>Tratamento de Gestos</Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    textTouchable: {
        fontWeight: '500', 
        textAlign: 'center',
        fontSize: 18
    },  
    touchable: {
        margin: 'auto',
        borderWidth: 1,
        borderColor: '#7A7A7A',
        width: 250, 
        maxHeight: 40,
        borderRadius: 3, 
        padding: 3,
        marginTop: 10
    },
});
export default HomeScreen;